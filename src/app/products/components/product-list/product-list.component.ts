import { Component, OnInit } from "@angular/core";
import { Product } from "../../models/product";
import { ProductService } from "../../services/product.service";
import { Router } from "@angular/router";
import { ProductState } from '../../store/product.reducer';
import { Store } from '@ngrx/store';
import * as fromActions from '../../store/product.actions';
import { selectProducts } from '../../store/product.selectors';
import { Observable, combineLatest } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.scss"],
})
export class ProductListComponent implements OnInit {
  products$: Observable<Product[]>
  form: FormGroup;

  constructor(
    private productService: ProductService,
    public router: Router,
    private store:Store<ProductState>,
    private fb: FormBuilder,
    ) {}

  ngOnInit() {

    this.form = this.fb.group({
      query: this.fb.control('', [Validators.required]),
    });

    this.store.dispatch(fromActions.loadProducts())
    this.loadProducts();

  }

  loadProducts() {
    const products = this.store.select(selectProducts);
    const query = this.query.valueChanges.pipe(
      startWith(this.query.value)
    );

    this.products$ = combineLatest([query, products]).pipe(
      // map(([query, products]) => {
      //   const _products = products.filter(product => {

      //   });

      //   return _products;
      // })
      map(([query, products]) => products.filter(
        (product: Product) => product.name.toLowerCase().indexOf(query.toLowerCase()) !== -1)),
    );
    
  }

  deleteProduct(id: string) {
    this.store.dispatch(fromActions.deleteProduct({id}));
  }

  get query() {
    return this.form.get('query');
  }
}
